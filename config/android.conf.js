
const androidInfo = require('../src/info/properties');
const {config} = require('./wdio.conf');


config.capabilities = [
    {
        platformName: 'Android',
        browserName: 'chrome',
        'appium:browserName' : 'chrome',
        maxInstances: 1,
        automationName: 'uiautomator2',
        deviceName: androidInfo.deviceName(),
        platformVersion: androidInfo.platFormVersion(),
        'cjson:metadata': {
            browser: {
                name: 'chrome',
                version: androidInfo.chromeVersion(),
            },
            device: androidInfo.deviceName(),
            platform: {
                 name: 'Android',
                    version: androidInfo.platFormVersion()
            }
        }

    }
];

config.services = [
    ['appium', {
        command : 'appium',
        args: {
            debugLogSpacing: true,
            platformName: "android",
            'chromedriver-executable': './node_modules/chromedriver/bin/chromedriver'
        }
    }]
],

config.cucumberOpts.tagExpression = '@android';

config.cucumberOpts.require = [
    './src/steps/android/*.js'
],

exports.config = config;
