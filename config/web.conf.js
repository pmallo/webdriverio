
const info = require('../src/info/properties');
const {config} = require('./wdio.conf');

config.capabilities = [{
    browserName: 'chrome',
    'cjson:metadata': {
        device: info.pcDeviceName(),
        platform: {
                version: info.pcOSName()
        }
    }
}
]

config.services = [
    'chromedriver'
],

config.cucumberOpts.tagExpression = '@web';

config.cucumberOpts.require = [
    './src/steps/web/*.js'
],

exports.config = config;
