
const { When } = require('cucumber');
const expectChai = require('chai').expect;

const homePage = new (require('../../pageobjects/android/homePage'));

When(/^I am open Ebay$/, function () {
    
    homePage.open()
    expectChai(homePage.pageIsLoaded()).to.equal(true);
});

When(/^I find "([^"]*)"$/, function (product) {
    homePage.search(product);
});
