
class resultPage {
   
    
    get resultField () { return $('//h1[@class="srp-controls__count-heading"]') }
    
    getResult () {
        return this.resultField.getText()
    }
}

module.exports = resultPage;
