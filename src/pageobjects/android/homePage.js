const basePage = require('./basePage');
const properties = require('../../info/properties.js')

class homePage extends basePage {
   
    get inputFind () { return $('#kw') }
    get btnSubmit () { return $('//*[input[@id="kw"]]/button[2]') }
    
    search (product) {
       this.inputFind.waitForDisplayed(5000)
       this.inputFind.clearValue()
       this.inputFind.setValue(product)
       this.btnSubmit.click()
    }

    open () {
       super.openURL(properties.urlEbay())
    }
 
    pageIsLoaded() {
      return this.inputFind.isDisplayed()
    }
}

module.exports = homePage;
